
CREATE TABLE region(
    id_Region    INT             AUTO_INCREMENT,
    Name         VARCHAR(250)    NOT NULL,
    OKATO        CHAR(6),
    PRIMARY KEY (id_Region)
)ENGINE=INNODB
;



CREATE TABLE sub_level(
    id_Sub_level             INT               AUTO_INCREMENT,
    id_Region                INT               NOT NULL,
    StartDate                DATE              NOT NULL,
    Sum_Common               DECIMAL(10, 0),
    Sum_Employable           DECIMAL(10, 0),
    Sum_Infant               DECIMAL(10, 0),
    Sum_Pensioner            DECIMAL(10, 0),
    Reglament_Title          VARCHAR(250),
    Reglament_Date           DATE,
    Reglament_Url            TEXT,
    Source_ContractNumber    VARCHAR(20),
    PRIMARY KEY (id_Sub_level)
)ENGINE=INNODB
;



CREATE TABLE tbl_migration(
    MigrationNumber    VARCHAR(250)    NOT NULL,
    MigrationName      VARCHAR(250)    NOT NULL,
    MigrationTime      TIMESTAMP       NOT NULL,
    PRIMARY KEY (MigrationNumber)
)ENGINE=INNODB
;



CREATE UNIQUE INDEX byName ON region(Name)
;
CREATE UNIQUE INDEX byOKATO ON region(OKATO)
;
CREATE INDEX Ref_Sub_level_Region ON sub_level(id_Region)
;
ALTER TABLE sub_level ADD CONSTRAINT Ref_Sub_level_Region 
    FOREIGN KEY (id_Region)
    REFERENCES region(id_Region)
;



@echo *******************************************************

@echo prepare create_sql
@call %~dp0\create_sql.bat

@set DBName=sldevel
@echo prepare empty database %DBName%
@set SQL_DROP_CREATE=drop database %DBName%; create database %DBName% default character set utf8;
@echo %SQL_DROP_CREATE% | %~dp0\run_mysql.bat
@if NOT 0==%ERRORLEVEL% exit rem stop on ERROR!

@echo execute create.sql
@call %~dp0\run_mysql.bat < %~dp0\sql\create\create.sql
@if NOT 0==%ERRORLEVEL% exit rem stop on ERROR!

@echo execute sl.etalon.sql 
@call %~dp0\run_mysql.bat < %~dp0\..\..\uc\forms\sl\tests\sl.etalon.sql 
@if NOT 0==%ERRORLEVEL% exit rem stop on ERROR!

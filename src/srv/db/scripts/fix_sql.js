if (WScript.FullName.indexOf("cscript.exe")==-1)
{
  WScript.Echo("This script may be runned only with CScript..  Try to run create_sql.bat")
  WScript.Quit(1)
}

var fso = new ActiveXObject("Scripting.FileSystemObject");
var filename= WScript.Arguments(0);

function flush_constraints(new_f,constraints)
{
  constraints.sort();
  for (var i= 0; i<constraints.length; i++)
  {
    var line= constraints[i];
    if (","==line.charAt(line.length-1))
      line= line.substr(0,line.length-1);
    if (i!=constraints.length-1)
      line= line + ",";
    new_f.WriteLine(line);
  }
}

function flush_fields(new_f, fields) 
{
	fields.sort();
	for (var i = 0; i < fields.length; i++)
	{
		var line = fields[i];
		new_f.WriteLine(line);
	}
}

function flush_keys(new_f, keys) 
{
	keys.sort();
	for (var i = 0; i < keys.length; i++)
	{
		var line= keys[i];
		if (","==line.charAt(line.length-1))
		  line= line.substr(0,line.length-1);
		if (i!=keys.length-1)
		  line= line + ",";
		new_f.WriteLine(line);
	}
}

function fix_sql_line(line)
{
  var prefix= "AUTO_INCREMENT=";
  var i= line.indexOf(prefix);
  if (-1 == i)
  {
    return line;
  }
  else
  {
    var i1= line.indexOf(" ",i);
    return line.substr(0,i) + line.substr(i1+1);
  }
  return line;
}

function fix_sql_xml(filename)
{
  var sprefix = "  CONSTRAINT";
  var kPrefix = "  KEY";
  var fPrefix = "  `";
  var f= fso.OpenTextFile(filename, 1);
  var new_filename= filename + ".new";
  var new_f= fso.CreateTextFile(new_filename);
  var constraints= [];
  var fields = [];
  var keys = [];
  
  while (!f.AtEndOfStream)
  {
    var line= f.ReadLine();
    if (0 != line.indexOf("-- ") &&
        0 != line.indexOf("CREATE DATABASE ") &&
        0 != line.indexOf("USE `") &&
		0 != line.indexOf("/*!50003 SET sql_mode              = '"))
    {
      var i= line.indexOf(sprefix);
      if (-1 != i)
      {
        constraints.push(line);
      }
      else
      {
        i= line.indexOf(kPrefix);
        if (-1 != i)
        {
          keys.push(line);
        }
        else
        {
          i= line.indexOf(fPrefix);
          if (-1 != i)
          {
            fields.push(line);
          }
          else
          {
            flush_fields(new_f,fields);
            flush_keys(new_f,keys);
            flush_constraints(new_f,constraints);
            constraints= [];
            fields= [];
            keys= [];
            new_f.WriteLine(fix_sql_line(line));
          }
        }
      }
    }
  }
  f.Close();
  new_f.Close();
  var old_filename= filename + ".old";
  fso.MoveFile(filename,old_filename);
  fso.MoveFile(new_filename,filename);
  fso.DeleteFile(old_filename);
}

fix_sql_xml(filename)
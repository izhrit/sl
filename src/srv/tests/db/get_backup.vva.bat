@set MYSQLDIR=C:\Program Files\MySQL\MySQL Server 5.7\bin\
@set MYSQLHOST="localhost"
@set DBName="sldevel"

call "%MYSQLDIR%\mysqldump.exe" --defaults-extra-file=%~dp0\..\..\db\mysql.conf --hex-blob --host=%MYSQLHOST% --default-character-set=utf8 --set-charset --routines --no-autocommit --extended-insert --result-file=backup.%date%.%random%.sql %DBName%
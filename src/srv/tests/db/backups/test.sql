-- MySQL dump 10.13  Distrib 5.7.28, for Win64 (x86_64)
--
-- Host: localhost    Database: sldevel
-- ------------------------------------------------------
-- Server version	5.7.28-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `id_Region` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(250) NOT NULL,
  `OKATO` char(6) DEFAULT NULL,
  PRIMARY KEY (`id_Region`),
  UNIQUE KEY `byName` (`Name`),
  UNIQUE KEY `byOKATO` (`OKATO`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region`
--

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `region` VALUES (1,'Алтайский край','01 000'),(2,'Краснодарский край','03 000'),(3,'Красноярский край','04 000'),(4,'Приморский край','05 000'),(5,'Ставропольский край','07 000'),(6,'Хабаровский край','08 000'),(7,'Амурская область','10 000'),(8,'Архангельская область','11 000'),(9,'Ненецкий автономный округ (Архангельская область)','11 100'),(10,'Астраханская область','12 000'),(11,'Белгородская область','14 000'),(12,'Брянская область','15 000'),(13,'Владимирская область','17 000'),(14,'Волгоградская область','18 000'),(15,'Вологодская область','19 000'),(16,'Воронежская область','20 000'),(17,'Нижегородская область','22 000'),(18,'Ивановская область','24 000'),(19,'Иркутская область','25 000'),(20,'Республика Ингушетия','26 000'),(21,'Калининградская область','27 000'),(22,'Тверская область','28 000'),(23,'Калужская область','29 000'),(24,'Камчатский край','30 000'),(25,'Кемеровская область - Кузбасс','32 000'),(26,'Кировская область','33 000'),(27,'Костромская область','34 000'),(28,'Республика Крым','35 000'),(29,'Самарская область','36 000'),(30,'Курганская область','37 000'),(31,'Курская область','38 000'),(32,'Город Санкт-Петербург','40 000'),(33,'Ленинградская область','41 000'),(34,'Липецкая область','42 000'),(35,'Магаданская область','44 000'),(36,'Город Москва','45 000'),(37,'Московская область','46 000'),(38,'Мурманская область','47 000'),(39,'Новгородская область','49 000'),(40,'Новосибирская область','50 000'),(41,'Омская область','52 000'),(42,'Оренбургская область','53 000'),(43,'Орловская область','54 000'),(44,'Пензенская область','56 000'),(45,'Пермский край','57 000'),(46,'Псковская область','58 000'),(47,'Ростовская область','60 000'),(48,'Рязанская область','61 000'),(49,'Саратовская область','63 000'),(50,'Сахалинская область','64 000'),(51,'Свердловская область','65 000'),(52,'Смоленская область','66 000'),(53,'Город федерального значения Севастополь','67 000'),(54,'Тамбовская область','68 000'),(55,'Томская область','69 000'),(56,'Тульская область','70 000'),(57,'Тюменская область','71 000'),(58,'Ульяновская область','73 000'),(59,'Челябинская область','75 000'),(60,'Забайкальский край','76 000'),(61,'Чукотский автономный округ','77 000'),(62,'Ярославская область','78 000'),(63,'Республика Адыгея (Адыгея)','79 000'),(64,'Республика Башкортостан','80 000'),(65,'Республика Бурятия','81 000'),(66,'Республика Дагестан','82 000'),(67,'Кабардино-Балкарская Республика','83 000'),(68,'Республика Алтай','84 000'),(69,'Республика Калмыкия','85 000'),(70,'Республика Карелия','86 000'),(71,'Республика Коми','87 000'),(72,'Республика Марий Эл','88 000'),(73,'Республика Мордовия','89 000'),(74,'Республика Северная Осетия-Алания','90 000'),(75,'Карачаево-Черкесская Республика','91 000'),(76,'Республика Татарстан (Татарстан)','92 000'),(77,'Республика Тыва','93 000'),(78,'Удмуртская Республика','94 000'),(79,'Республика Хакасия','95 000'),(80,'Чеченская Республика','96 000'),(81,'Чувашская Республика - Чувашия','97 000'),(82,'Республика Саха (Якутия)','98 000'),(83,'Еврейская автономная область','99 000'),(84,'Ханты-Мансийский автономный округ - Югра','71 100'),(85,'Ямало-Ненецкий автономный округ','71 140'),(86,'Российская Федерация','00 000');
/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `sub_level`
--

DROP TABLE IF EXISTS `sub_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_level` (
  `id_Sub_level` int(11) NOT NULL AUTO_INCREMENT,
  `id_Region` int(11) NOT NULL,
  `StartDate` date NOT NULL,
  `Sum_Common` decimal(10,0) DEFAULT NULL,
  `Sum_Employable` decimal(10,0) DEFAULT NULL,
  `Sum_Infant` decimal(10,0) DEFAULT NULL,
  `Sum_Pensioner` decimal(10,0) DEFAULT NULL,
  `Reglament_Title` varchar(250) DEFAULT NULL,
  `Reglament_Date` date DEFAULT NULL,
  `Reglament_Url` text,
  `Source_ContractNumber` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_Sub_level`),
  KEY `Ref_Sub_level_Region` (`id_Region`),
  CONSTRAINT `Ref_Sub_level_Region` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_level`
--

LOCK TABLES `sub_level` WRITE;
/*!40000 ALTER TABLE `sub_level` DISABLE KEYS */;
set autocommit=0;
INSERT INTO `sub_level` VALUES (1,78,'2020-01-01',10010,10020,10030,10040,'Приказ №1 по округу','2019-11-01','http://google.com','1'),(3,75,'2019-01-01',11000,12000,13000,14000,'Приказ №2 по раёну','2018-02-01','http://yandex.com',NULL);
/*!40000 ALTER TABLE `sub_level` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `tbl_migration`
--

DROP TABLE IF EXISTS `tbl_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_migration` (
  `MigrationNumber` varchar(250) NOT NULL,
  `MigrationName` varchar(250) NOT NULL,
  `MigrationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`MigrationNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_migration`
--

LOCK TABLES `tbl_migration` WRITE;
/*!40000 ALTER TABLE `tbl_migration` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `tbl_migration` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping routines for database 'sldevel'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-26 13:59:43

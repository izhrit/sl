<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';

$fields= "
	 sl.id_Sub_level id
	,rg.Name Регион
	,date_format(sl.StartDate,'%d.%m.%Y') Дата

	,sl.Sum_Common Общий
	,sl.Sum_Employable Трудоспособных
	,sl.Sum_Pensioner Пенсионеров
	,sl.Sum_Infant Несовершеннолетних

	,if(sl.Source_ContractNumber is not null,0,1) Публичность
";

$from_where= " from sub_level sl inner join region rg on rg.id_Region=sl.id_Region ";

$filter_rule_builders= array(
	'Регион'=> prep_std_filter_rule_builder_for_expression('rg.Name')
	,'Дата'=> prep_std_filter_rule_builder_for_expression('date_format(sl.StartDate,\'%d.%m.%Y\')')

	,'Общий'=> prep_std_filter_rule_builder_for_expression('sl.Sum_Common')
	,'Трудоспособных'=>prep_std_filter_rule_builder_for_expression('sl.Sum_Employable')
	,'Пенсионеров'=>prep_std_filter_rule_builder_for_expression('sl.Sum_Pensioner')
	,'Несовершеннолетних'=>prep_std_filter_rule_builder_for_expression('sl.Sum_Infant')

	,'Публичность'=>prep_std_filter_rule_builder_for_expression('if(sl.Source_ContractNumber is not null,0,1)')
);
execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders);

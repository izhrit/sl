<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';

class Sub_level_crud extends Base_crud
{
	function create($sl)
	{
		$txt_query= "insert into sub_level set
			id_Region=(select region.id_Region from region where OKATO=?)
			,StartDate=?

			,Sum_Common=?
			,Sum_Employable=?
			,Sum_Pensioner=?
			,Sum_Infant=?

			,Reglament_Title=?
			,Reglament_Date=?
			,Reglament_Url=?
			;
		";
		$sl= (object)$sl;
		$sum= (object)$sl->Прожиточный_минимум;
		$rg= (object)$sl->Регламентирующий_акт;
		$affected= execute_query_get_affected_rows($txt_query,array('ssiiiisss',
			$sl->Регион,date_format(date_create_from_format('d.m.Y',$sl->Дата),'Y-m-d')
			,(!$sum->Подушевой['Задан'] ? null : $sum->Подушевой['Значение'])
			,(!$sum->Трудоспособных['Задан'] ? null : $sum->Трудоспособных['Значение'])
			,(!$sum->Пенсионеров['Задан'] ? null : $sum->Пенсионеров['Значение'])
			,(!$sum->Несовершеннолетних['Задан'] ? null : $sum->Несовершеннолетних['Значение'])
			,$rg->Название
			,(''==$rg->Дата ? null : date_format(date_create_from_format('d.m.Y',$rg->Дата),'Y-m-d'))
			,$rg->Ссылка
		));
		if (1!=$affected)
			exit_not_found("can not insert into sub_level (affected $affected rows)");
		echo '{ "ok": true }';
	}

	function read($id_Sub_level)
	{
		$txt_query= "select
			date_format(sl.StartDate,'%d.%m.%Y') StartDate
			,rg.OKATO

			,sl.Sum_Common
			,sl.Sum_Employable
			,sl.Sum_Pensioner
			,sl.Sum_Infant

			,sl.Reglament_Title
			,date_format(sl.Reglament_Date,'%d.%m.%Y') Reglament_Date
			,sl.Reglament_Url

		from sub_level sl inner join region rg on sl.id_Region=rg.id_Region
		where sl.id_Sub_level=?;";

		$rows= execute_query($txt_query,array('s',$id_Sub_level));
		if (0==count($rows))
			exit_not_found("can not find sub_level id_Sub_level=$id_Sub_level");
		$row= $rows[0];
		$sl= array
		(
			'Дата'=>$row->StartDate
			,'Регион'=> $row->OKATO
			,'Прожиточный_минимум'=> array
			(
				'Подушевой'=>array('Задан'=>null!=$row->Sum_Common, 'Значение'=>$row->Sum_Common)
				,'Трудоспособных'=>array('Задан'=>null!=$row->Sum_Employable, 'Значение'=>$row->Sum_Employable)
				,'Пенсионеров'=>array('Задан'=>null!=$row->Sum_Pensioner, 'Значение'=>$row->Sum_Pensioner)
				,'Несовершеннолетних'=>array('Задан'=>null!=$row->Sum_Infant, 'Значение'=>$row->Sum_Infant)
			)
			,'Регламентирующий_акт'=> array
			(
				'Название'=>$row->Reglament_Title
				,'Дата'=>$row->Reglament_Date
				,'Ссылка'=>$row->Reglament_Url
			)
		);
		echo nice_json_encode($sl);
	}

	function update($id_Sub_level,$sl)
	{
		$txt_query= "update sub_level set
			id_Region=(select region.id_Region from region where OKATO=?)
			,StartDate=?

			,Sum_Common=?
			,Sum_Employable=?
			,Sum_Pensioner=?
			,Sum_Infant=?

			,Reglament_Title=?
			,Reglament_Date=?
			,Reglament_Url=?

			where id_Sub_level=?
			;
		";
		$sl= (object)$sl;
		$sum= (object)$sl->Прожиточный_минимум;
		$rg= (object)$sl->Регламентирующий_акт;
		$affected= execute_query_get_affected_rows($txt_query,array('ssiiiissss',
			$sl->Регион,date_format(date_create_from_format('d.m.Y',$sl->Дата),'Y-m-d')
			,(!$sum->Подушевой['Задан'] ? null : $sum->Подушевой['Значение'])
			,(!$sum->Трудоспособных['Задан'] ? null : $sum->Трудоспособных['Значение'])
			,(!$sum->Пенсионеров['Задан'] ? null : $sum->Пенсионеров['Значение'])
			,(!$sum->Несовершеннолетних['Задан'] ? null : $sum->Несовершеннолетних['Значение'])
			,$rg->Название
			,(''==$rg->Дата ? null : date_format(date_create_from_format('d.m.Y',$rg->Дата),'Y-m-d'))
			,$rg->Ссылка
			,$id_Sub_level
		));
		if (1!=$affected)
			exit_not_found("can not update sub_level (affected $affected rows)");
		echo '{ "ok": true }';
	}

	function delete($id_Sub_levels)
	{
		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
		$connection= default_dbconnect();
		$connection->begin_transaction();
		foreach ($id_Sub_levels as $id_Sub_level)
		{
			$txt_query= "delete from sub_level where id_Sub_level=?;";
			$affected= $connection->execute_query_get_affected_rows($txt_query,array('i',$id_Sub_level));
			if (1!=$affected)
			{
				$connection->rollback();
				exit_not_found("can not delete sub_level where id_Sub_level=$id_Sub_level (affected $affected rows)");
			}
		}
		$connection->commit();
		echo '{ "ok": true }';
	}
}

$crud= new Sub_level_crud();
$crud->process_cmd();


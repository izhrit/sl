// node optimizers\r.js -o baseUrl=. name=optimizers/almond include=forms/sl/extension out=..\built\sl.js wrap=true
({
    baseUrl: "..",
    include: ['forms/sl/e_sl'],
    name: "optimizers/almond",
    out: "..\\built\\sl.js",
    wrap: true,
    map:
    {
      '*':
      {
        tpl: 'js/libs/tpl',
        txt: 'js/libs/txt'
      }
    }
})
function GetSelectedSpecificationFor_id(specs, id)
{
	var id_parts = id.split('.');
	var namespace = id_parts[0];
	var name = id_parts[1];
	return specs[namespace][name];
}

function GetSelectedFormSpecificationFor_id_Form(form_id)
{
	return GetSelectedSpecificationFor_id(CPW_forms, form_id);
}

function GetSelectedFormSpecification()
{
	var form_selector = $('#cpw-form-select');
	var form_id = form_selector.val();
	return GetSelectedFormSpecificationFor_id_Form(form_id);
}

function CreateNewFormWithSpecFunc_created(form_spec, sel)
{
	app.current_form_spec = form_spec;
	StartEditor();
	form_spec.CreateNew(sel);
}

function do_with_controller(form_spec,on_controller)
{
	if (form_spec.CreateNew)
	{
		on_controller(form_spec);
	}
	else
	{
		form_spec.done(function (required_form_spec_func)
		{
			on_controller(required_form_spec_func());
		});
	}
}

function CreateNewFormWithSpecFunc(form_spec_func, sel)
{
	if (!sel)
		sel = '#report-editor';

	do_with_controller(form_spec_func(), function (form_spec)
	{
		CreateNewFormWithSpecFunc_created(form_spec, sel);
	});
}

function CreateNewForm()
{
	var form_spec_func = GetSelectedFormSpecification();

	var form_selector = $('#cpw-form-select');
	var form_id = form_selector.val();

	var href = window.location.href;
	var args_pos = href.lastIndexOf('?');
	var prefix = href.substring(0, args_pos);
	window.history.pushState(/*state:*/"", document.title, prefix + "?createnew=" + form_id);

	CreateNewFormWithSpecFunc(form_spec_func);
}

function EditFormWithSpecFunc_created(form_spec, sel, form_content)
{
	var content_checking_text = form_spec.SetFormContent(form_content);
	if (null != content_checking_text)
	{
		alert("содержимое НЕ является валидным документом:\r\n\r\n" + content_checking_text);
	}
	else
	{
		form_spec.Edit(sel);
		app.current_form_spec = form_spec;
		StartEditor();
	}
}

function EditFormWithSpecFunc(form_spec_func, sel, form_content)
{
	if (!sel)
		sel = '#report-editor';

	if (!form_content)
		form_content = $('#form-content-text-area').val();

	do_with_controller(form_spec_func(), function (form_spec)
	{
		EditFormWithSpecFunc_created(form_spec, sel, form_content);
	});
}

function EditForm()
{
	var form_spec_func = GetSelectedFormSpecification();

	var form_selector = $('#cpw-form-select');
	var form_id = form_selector.val();

	var content_id = $('#selected_content_id').text();
	var args_pos = content_id.lastIndexOf('.');
	var postfix = content_id.substring(args_pos);

	var href = window.location.href;
	var args_pos = href.lastIndexOf('?');
	var prefix = href.substring(0, args_pos);
	window.history.pushState(/*state:*/"", document.title, prefix + "?edit=" + form_id + postfix);

	EditFormWithSpecFunc(form_spec_func);
}

function ViewFormWithSpecFunc(form_spec_func)
{
	var form_spec = form_spec_func();
	var form_content = $('#form-content-text-area').val();

	var content_checking_text = form_spec.SetFormContent(form_content);
	if (null != content_checking_text)
	{
		alert("содержимое НЕ является валидным документом:\r\n\r\n" + content_checking_text);
	}
	else
	{
		/*form_spec.View('report-editor');
		app.current_form_spec = form_spec;
		StartView();*/

		var xml_view_text = form_spec.BuildXamlView();
		var xml_Blob = new Blob([xml_view_text], { type: "text/xaml;charset=utf-8;" });
		saveAs(xml_Blob, "form.xaml");
	}
}

function PrepareXamlFor(id_form,id_content)
{
	var form_content = GetSelectedSpecificationFor_id(CPW_contents, id_content);
	var form_spec_func = GetSelectedFormSpecificationFor_id_Form(id_form);
	var form_spec = form_spec_func();

	form_spec.SetFormContent(form_content);
	return form_spec.BuildXamlView();
}

function ViewForm()
{
	var form_spec_func = GetSelectedFormSpecification();
	ViewFormWithSpecFunc(form_spec_func);
}

function StartEditor()
{
	$('#form-management').hide();
	$('#btn_test_forms_SaveForm').show();
	$('#form-editor-footer-buttons').show();
}

function StartView()
{
	$('#form-management').hide();
	$('#btn_test_forms_SaveForm').hide();
	$('#form-editor-footer-buttons').show();
}

function StopEditor()
{
	$('#form-management').show();
	$('#form-editor-footer-buttons').hide();
	$('#report-editor').empty();
	app.current_form_spec = null;
}

function CancelEditWithoutSave()
{
	StopEditor();
}

function MessageBox(html, title)
{
	$("#dialog").html(html);
	$("#dialog").dialog({
		width: 800
		, title: title ? title : 'Быстрое текстовое сообщение'
		, buttons: { "OK": function () { $(this).dialog("close"); } }
	});
}

function IfOkWithValidateResult(validation_result, on_validate_ok_func)
{
	var title_reject = 'Отказ сохранять содержимое формы из-за нарушенных органичений';
	if (null == validation_result)
	{
		on_validate_ok_func();
	}
	else if ('string' == typeof validation_result)
	{
		MessageBox('<pre>' + validation_result + '</pre>', title_reject);
	}
	else if ('[object Array]' === Object.prototype.toString.call(validation_result))
	{
		var html = '';
		var invalid = false;
		var descriptions = '';
		for (var i = 0; i < validation_result.length; i++)
		{
			var validate_constraint= validation_result[i];
			if (false == validate_constraint.check_constraint_result)
				invalid = true;
			descriptions+= '<li>' + validate_constraint.description + '</li>';
		}
		if (invalid)
		{
			MessageBox('<ul>' + descriptions + '</ul>', title_reject);
		}
		else
		{
			$("#dialog").html('<ul style="padding-left: 1.5em;">' + descriptions + '</ul><div style="padding-left: 1.5em;">Сохранить содержимое формы?</div>');
			$("#dialog").dialog({
				width: 800
				, title: 'Предупреждение перед сохранением содержимого формы'
				, buttons:
					{
						"Да, сохранить": function () { $(this).dialog("close"); on_validate_ok_func(); }
						,"Нет, вернуться к редактированию": function () { $(this).dialog("close"); }
					}
			});
		}
	}
	else
	{
		MessageBox('Валидация завершена каким то непредсказуемым образом', title_reject);
	}
}

function SaveForm()
{
	var form_spec = app.current_form_spec;
	IfOkWithValidateResult(form_spec.Validate(),
	function ()
	{
		var form_content = form_spec.GetFormContent();
		if (null != form_content)
		{
			if ("string" != typeof form_content)
				form_content = JSON.stringify(form_content, null, "\t");
			$('#form-content-text-area').val(form_content);
			StopEditor();
		}
	});
}

function ChangeContentVariant()
{
	var content_selector = $('#cpw-content-select');
	var content_id = content_selector.val();
	var content = GetSelectedSpecificationFor_id(CPW_contents, content_id);

	var content_area = $('#form-content-text-area');
	var old_content = content_area.val();
	if (!old_content || '' == old_content || confirm('Вы действительно хотите заменить содержимое новым?'))
	{
		$('#selected_content_id').text(content_id);
		if ('string' == typeof content)
		{
			$('#form-content-text-area').val(content);
		}
		else
		{
			content().done(function (required_content)
			{
				$('#form-content-text-area').val(required_content);
			});
		}
	}

	content_selector.val('');
}

function Print()
{
	var content = app.current_form_spec.BuildHtmlContentToPrint();

	$('#page').addClass('non-printable');
	$('body')
	.addClass('body-printable')
	.append('<div id="printable" class="printable">' + content + '</div>');

	window.print();

	$('#printable').detach();
	$('body').removeClass('body-printable');
	$('#page').removeClass('non-printable');
}

$(function ()
{
	$("#btn_test_forms_CreateNew").click(CreateNewForm);
	$("#btn_test_forms_CancelEdit").click(CancelEditWithoutSave);
	$('#btn_test_forms_SaveForm').click(SaveForm);
	$('#btn_test_forms_Print').click(Print);
	$('#btn_test_forms_Edit').click(EditForm);
	$('#btn_test_forms_View').click(ViewForm);

	var templateSelection = function (state)
	{
		var res = '';
		res += '<span>';
		res += state.id;
		var title = $(state.element).data('title');
		if (title)
			res += ' <small style="color:gray">(' + title + ')</small>';
		res += '</span>';
		return res;
	}

	var templateResult = function (state)
	{
		var res = '';
		res += '<div style="display:inline-block;border-bottom:1px dotted silver;width:100%;">';
		res += '<div style="display:inline-block;">';
		res += state.id;
		var title = $(state.element).data('title');
		if (title)
			res += ' <small>(' + title + ')</small>';
		res += '</div>';
		var keywords = $(state.element).data('keywords');
		if (keywords)
		{
			res += '<div style="display:inline-block;float:right;font-size:0.8em;">';
			res += '<small>tags: ' + keywords+'</small>';
			res += '</div>';
		}
		res += '</div>';
		return res;
	}

	$('#cpw-form-select').select2({formatResult: templateResult, formatSelection: templateSelection, escapeMarkup: function (m){return m;} });
	$('#cpw-content-select').select2({ formatResult: templateResult, formatSelection: templateSelection, escapeMarkup: function (m) { return m; } });

	$('#cpw-content-select').change(ChangeContentVariant);

	var queryString = window.location.search;
	if (-1 != queryString.indexOf('&demo'))
	{
		$('body').addClass('demo');
	}
});

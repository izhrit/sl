require.config
({
	enforceDefine: true,
	urlArgs: "bust" + (new Date()).getTime(),
	baseUrl: '.',
	paths:
	{
		styles: 'css',
		images: 'images'
	},
	map:
	{
		'*':
		{
			tpl:   'js/libs/tpl',
			css:   'js/libs/css',
			image: 'js/libs/image',
			txt:   'js/libs/txt'
		}
	}
});

require([
	  'forms/base/fastened/test/h_fastened_field_testers'
], function (h_fastened_field_testers)
{
	wbt = {};

	wbt.SetModelFieldValue= function(model_selector, value)
	{
		var sel = this.Model_selector_prefix + '[model-selector="' + model_selector + '"]';
		return wbt.ActionBySpec(sel, function (dom_item) { return wbt.SetValue(dom_item, value, sel); });
	}

	wbt.CheckModelFieldValue = function (model_selector, value)
	{
		var sel = this.Model_selector_prefix + '[model-selector="' + model_selector + '"]';
		return wbt.ActionBySpec(sel, function (dom_item) { return wbt.CheckValue(dom_item, value); });
	}

	wbt.Model_selector_prefix = '';

	wbt.Push_workspace= function(selector)
	{
		this.prev_context =
			{
				prev_selector: this.prev_selector
				, Model_selector_prefix: this.Model_selector_prefix
			};
		this.Model_selector_prefix += selector + ' ';
		return (window.wbt_CheckMode) ? null : 'current selector prefix is \"' + this.Model_selector_prefix + '\"';
	}

	wbt.Pop_workspace= function()
	{
		this.Model_selector_prefix = !this.prev_context || null == this.prev_context ? '' : this.prev_context.Model_selector_prefix;
		this.prev_context = !this.prev_context || null == this.prev_context ? null : this.prev_context.prev_context;
		return (window.wbt_CheckMode) ? null : 'current selector prefix is \"' + this.Model_selector_prefix + '\"';
	}

	wbt.Push_workspace_model_selector= function(model_selector)
	{
		return this.Push_workspace('[model-selector="' + model_selector + '"]');
	}

	wbt.ActionBySpec = function (sel, action)
	{
		try
		{
			var dom_item = $(sel);
			if (!dom_item || null == dom_item || 0 == dom_item.length)
			{
				return 'can not find element for selector "' + sel + '"';
			}
			else
			{
				var res = action(dom_item);
				return null == res ? null :
					'wait_long_process!' == res ? res : '\'' + sel + '\': ' + res;
			}
		}
		catch (ex)
		{
			return 'exception ' + ex.toString();
		}
	}

	wbt.SetValue = function (dom_item, value, model_selector)
	{
		app.wbt_long_process_result = null;
		var field_tester = h_fastened_field_testers.find_tester_for(dom_item);
		if (!window.wbt_CheckMode)
		{
			return field_tester.set_value(dom_item, value, model_selector);
		}
		else
		{
			return field_tester.check_value(dom_item, value, /* instead_of_set= */true);
		}
	}

	wbt.CheckValue = function (dom_item, value)
	{
		var field_tester = h_fastened_field_testers.find_tester_for(dom_item);
		return field_tester.check_value(dom_item, value);
	}

	wbt.loaded_img_src= function(spec)
	{
		var img = $(spec);
		if (0 == img.length)
		{
			return false;
		}
		else
		{
			for (var i = 0; i < img.length; i++)
			{
				if (0 == img[i].naturalWidth)
					return false;
			}
			return true;
		}
	}

	wbt.loaded_background_image = function (spec)
	{
		var items = $(spec);
		if (0 == items.length)
		{
			return false;
		}
		else
		{
			for (var i = 0; i < items.length; i++)
			{
				var item = $(items[i]);
				var src = item.css('background-image');
				var match_result = src.match(/\((.*?)\)/);
				if (null != match_result && match_result.length > 1)
				{
					var url = match_result[1].replace(/('|")/g, '');
					var img = new Image();
					img.src = url;
					if (0 == img.naturalHeight)
						return false;
				}
			}
			return true;
		}
	}

	wbt.calc_loaded_fontFamily_width = function(font)
	{
		var res = { font: font };

		var node = document.createElement('span');
		// Characters that vary significantly among different fonts
		node.innerHTML = 'giItT1WQy@!-/#';
		// Visible - so we can measure it - but not on the screen
		node.style.position = 'absolute';
		node.style.left = '-10000px';
		node.style.top = '-10000px';
		// Large font size makes even subtle changes obvious
		node.style.fontSize = '300px';
		// Reset any font properties
		node.style.fontFamily = 'sans-serif';
		node.style.fontVariant = 'normal';
		node.style.fontStyle = 'normal';
		node.style.fontWeight = 'normal';
		node.style.letterSpacing = '0';
		document.body.appendChild(node);

		// Remember width with no applied web font
		res.sans_serif_width = node.offsetWidth;

		node.style.fontFamily = font;

		res.fontWidth= node.offsetWidth;

		node.parentNode.removeChild(node);
		node = null;

		return res;
	}

	wbt.get_fontFamily_width = function(font)
	{
		var res= this.calc_loaded_fontFamily_width(font);
		return res.fontWidth;
	}

	wbt.loaded_fontFamily = function (font,goal)
	{
		var res= this.calc_loaded_fontFamily_width(font);
		return !goal 
			? res.sans_serif_width!=res.fontWidth
			: goal==res.fontWidth;
	}

	wbt.rendered_tab_control = function (spec)
	{
		if (!this.loaded_background_image(spec))
			return false;
		var tabs = $(spec + ' li[role="tab"]');
		if (0 == tabs.length)
		{
			return false;
		}
		else
		{
			for (var i= 0; i<tabs.length; i++)
			{
				if (!this.loaded_background_image(tabs[i]))
					return false;
			}
			return true;
		}
	}

	wbt.jqgrid_has_highlighted_row = function (spec)
	{
		return (1 == ($(spec + ' tbody tr.ui-state-highlight').length));
	}

	wbt.jqgrid_selected_rows_count = function (spec)
	{
		return $(spec + ' tbody input.cbox::checked').length;
	}

	wbt.jqgrid_rows_count = function (spec)
	{
		return $(spec + ' tbody tr.jqgrow').length;
	}

	wbt.Select2Focus = function (id)
	{
		$('#s2id_' + id + ' .select2-focusser').focus();
		$('#s2id_' + id + ' .select2-choice').click().mousedown().mouseup();
		$('#s2id_' + id + ' .select2-focusser').focus();
		$('#' + id).select2('close');
	}

	wbt.SetStdTestTime= function()
	{
		app.cpw_Now = function ()
		{
			if (app.timer)
			{
				app.timer++;
			}
			else
			{
				app.timer = 0;
			}
			return new Date(new Date(2015, 6, 26, 17, 36, 0, 0).getTime() + app.timer);
		};
	}

	wbt.log = function (txt)
	{
		var ok_logged = false;
		if (!ok_logged)
			try { window.external.Log(txt); ok_logged = true; } catch (e) { }
		if (!ok_logged)
			try { console.log(txt); ok_logged = true; } catch (e) { }
		if (!ok_logged)
			try { WScript.Echo(txt); ok_logged = true; } catch (e) { }
	}

	wbt.transition_finished = function ()
	{
		return this.count_to_transition==this.count_transitioned;
	}

	wbt_CheckIfLongProcessFinished = function ()
	{
		if (null == app.wbt_long_process_result)
		{
			return "wait_long_process!";
		}
		else
		{
			var res = app.wbt_long_process_result;
			app.wbt_long_process_result = null;
			return res;
		}
	}

	wbt_controller_GetFormContentTextArea= function()
	{
		return $('#form-content-text-area').val().replace(new RegExp('\n','g'),'\r\n');
	}
});

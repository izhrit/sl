﻿define
(
	[
		  'forms/base/ajax/ajax-collector'

		, 'forms/sl/tests/ajax/sl/a_sl_jqgrid'
		, 'forms/sl/tests/ajax/sl/a_sl_crud'
	],
	function (ajax_collector)
	{
		return ajax_collector(Array.prototype.slice.call(arguments, 1));
	}
);
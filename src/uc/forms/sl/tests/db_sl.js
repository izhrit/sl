﻿define([
	'txt!forms/sl/tests/db_sl.json.txt'
	, 'forms/sl/tests/db_sl_read'
	, 'forms/sl/base/h_region'
],
function (content_ini_txt, db_sl_read, h_region)
{
	db_sl_read.content= JSON.parse(content_ini_txt);

	var region= [];

	for (var i = 0; i < h_region.length; i++)
	{
		var r= h_region[i];
		region.push({
			id_Region:i+1
			,Name:r.Наименование
			,OKATO:r.Код_ОКАТО
		});
	}

	db_sl_read.content.region= region;

	return db_sl_read.content;
});
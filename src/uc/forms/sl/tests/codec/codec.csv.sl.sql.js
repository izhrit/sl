define([
	  'forms/base/codec/codec'
	, 'forms/sl/base/h_region'
	, 'forms/base/codec/datetime/h_codec.datetime'
],
function (BaseCodec, h_region, h_codec_datetime)
{
	return function ()
	{
		var codec = BaseCodec();

		codec.Decode= function (data)
		{
			var lines= data.split("\n");
			var records= [];
			for (var i = 0; i < lines.length; i++)
			{
				var line= lines[i];
				var fields= line.split(';');
				if (fields.length > 1)
				{
					var sl_record= {
						Регион: 
						{ 
							Наименование: fields[0]
						}
						,Квартал:fields[1]
						,Дата: this.Квартал2Дата(fields[1])
						,Прожиточный_минимум:
						{
							Подушевой: fields[2]
							,Трудоспособных: fields[3]
							,Пенсионеров: fields[4]
							,Несовершеннолетних: fields[5]
						}
						,Регламентирующий_акт:
						{
							Документ: fields[6]
						}
					};
					if (!this.Fix_Регион(sl_record.Регион))
						return "can not prepare region '" + sl_record.Регион.Наименование + "'";
					this.Fix_Реглаиментирующий_акт(sl_record.Регламентирующий_акт);
					records.push(sl_record);
				}
			}
			return records;
		}

		var параметры_квартала_по_имени = {
			первый: { первый_день: '01.01.', номер:'1'}
			, второй: { первый_день: '01.04.', номер:'2'}
			, третий: { первый_день: '01.07.', номер:'3'}
			, четвертый: { первый_день: '01.10.', номер:'4'}
		};

		codec.Квартал2Дата = function (квартал)
		{
			var parts= квартал.split(' ');
			if (3!=parts.length)
				return 'нет трёх частей через пробел!';

			var имя_квартала= parts[1];
			if (!параметры_квартала_по_имени[имя_квартала])
				return 'неожиданная вторая часть для имени "' + имя_квартала + '"!';

			var параметры_квартала= параметры_квартала_по_имени[имя_квартала];

			if (параметры_квартала.номер!=parts[0])
				return 'первая часть не соответствует второй!';

			var год= parts[2];
			if (4!=год.length)
				return 'в годе не 4 цифры!';

			var год= parseInt(год);
			if (год<2017 || год>2020)
				return 'непонятный год!';

			return параметры_квартала.первый_день + год;
		}

		codec.Fix_Реглаиментирующий_акт = function (r)
		{
			var parts= r.Документ.split(' ');
			r.Дата= parts[0];
			r.Название= parts[1];
			if ('\r'==r.Название.charAt(r.Название.length-1))
				r.Название= r.Название.substring(0,r.Название.length-1);
		}

		codec.Стандартное_наименование_региона = 
		{
			'Бурятия Республика':'Республика Бурятия'
			,'Ингушетия Республика':'Республика Ингушетия'
			,'Калмыкия Республика':'Республика Калмыкия'
			,'Кемеровская область':'Кемеровская область - Кузбасс'
			,'Коми Республика':'Республика Коми'
			,'Крым Республика':'Республика Крым'
			,'Марий Эл Республика':'Республика Марий Эл'
			,'Мордовия Республика':'Республика Мордовия'
			,'Ненецкий автономный округ':'Ненецкий автономный округ (Архангельская область)'
			,'Саха (Якутия) Республика':'Республика Саха (Якутия)'
			,'Северная Осетия-Алания Республика':'Республика Северная Осетия-Алания'
			,'Татарстан Республика':'Республика Татарстан (Татарстан)'
			,'Хакасия Республика':'Республика Хакасия'
			,'Ханты-Мансийский автономный округ Югра':'Ханты-Мансийский автономный округ - Югра'
			,'Чувашская Республика':'Чувашская Республика - Чувашия'
			,'Ямало-Ненецкий автономный округ':'Ямало-Ненецкий автономный округ'
			,'Адыгея Республика':'Республика Адыгея (Адыгея)'
			,'Алтай Республика':'Республика Алтай'
			,'Башкортостан Республика':'Республика Башкортостан'
			,'Дагестан Республика':'Республика Дагестан'
			,'Карелия Республика':'Республика Карелия'
			,'Москва':'Город Москва'
			,'Санкт-Петербург':'Город Санкт-Петербург'
			,'Севастополь':'Город федерального значения Севастополь'
			,'Тыва Республика':'Республика Тыва'
		}

		codec.Fix_Регион = function (r)
		{
			for (var i = 0; i < h_region.length; i++)
			{
				var rr= h_region[i];
				var Наименование= r.Наименование;
				if (this.Стандартное_наименование_региона[Наименование])
					Наименование= this.Стандартное_наименование_региона[Наименование];
				if (rr.Наименование == Наименование)
				{
					r.Код_ОКАТО= rr.Код_ОКАТО;
					return true;
				}
			}
			return false;
		}

		codec.Encode_sum = function (v)
		{
			return 'РФ'==v ? 'null' : v.replace(",",".");
		}

		codec.Encode = function (items)
		{
			var dt_codec= h_codec_datetime.ru_legal_txt2txt_mysql();

			var res_txt_sql = 'set names utf8;\r\n\r\n';

			res_txt_sql += "select 'insert into sub_level' as '';\r\n";
			for (var i = 0; i < items.length; i++)
			{
				var sl= items[i];
				res_txt_sql += "insert into `sub_level` set ";

				res_txt_sql += "StartDate='" + dt_codec.Encode(sl.Дата) + "'";

				var v= sl.Прожиточный_минимум;
				res_txt_sql += ", Sum_Common=" + this.Encode_sum(v.Подушевой);
				res_txt_sql += ", Sum_Employable=" + this.Encode_sum(v.Трудоспособных);
				res_txt_sql += ", Sum_Infant=" + this.Encode_sum(v.Несовершеннолетних);
				res_txt_sql += ", Sum_Pensioner=" + this.Encode_sum(v.Пенсионеров);

				var rd= sl.Регламентирующий_акт;

				res_txt_sql += ", Reglament_Title='" + rd.Название + "'";
				res_txt_sql += ", Reglament_Date='" + dt_codec.Encode(rd.Дата) + "'";

				res_txt_sql += ", id_Region=(select r.id_Region from region r where r.OKATO='" + sl.Регион.Код_ОКАТО + "')";

				res_txt_sql += ";\r\n";
			}

			return res_txt_sql;
		}

		return codec;
	}
});

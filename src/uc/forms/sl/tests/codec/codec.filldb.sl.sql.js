define([
	  'forms/base/codec/codec.filldb.sql'
	, 'forms/sl/base/h_region'
],
function (BaseCodec, h_region)
{
	return function ()
	{
		var codec = BaseCodec();

		var base_Encode= codec.Encode;
		codec.Encode= function (data)
		{
			var region= [];
			for (var i = 0; i < h_region.length; i++)
			{
				var r= h_region[i];
				region.push({
					id_Region:i+1
					,Name:r.Наименование
					,OKATO:r.Код_ОКАТО
				});
			}
			var fixed_data= {region:region};
			for (var table_name in data)
				fixed_data[table_name]= data[table_name];
			return base_Encode.call(this,fixed_data);
		}

		return codec;
	}
});

﻿define([
	'forms/sl/tests/db_sl'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/codec/codec.copy'
	, 'forms/base/ql'
	, 'forms/base/codec/datetime/h_codec.datetime'
	, 'forms/base/h_times'
]
, function (db, ajax_CRUD, codec_copy, ql, h_codec_datetime, h_times)
{
	var transport = ajax_CRUD();

	transport.options.url_prefix = 'sl?action=sl.crud';

	var copy_to_db_record = function (sl_record, sl)
	{
		sl.StartDate= sl_record.Дата;

		var v= sl_record.Прожиточный_минимум;
		sl.Sum_Common= !v.Подушевой.Задан ? null : v.Подушевой.Значение;
		sl.Sum_Employable= !v.Трудоспособных.Задан ? null : v.Трудоспособных.Значение;
		sl.Sum_Pensioner= !v.Пенсионеров.Задан ? null : v.Пенсионеров.Значение;
		sl.Sum_Infant= !v.Несовершеннолетних.Задан ? null : v.Несовершеннолетних.Значение;

		var ract= sl_record.Регламентирующий_акт;
		sl.Reglament_Title= ract.Название;
		sl.Reglament_Date= ract.Дата;
		sl.Reglament_Url= ract.Ссылка;

		var region = ql.find_first_or_null(db.region, function (r) { return r.OKATO==sl_record.Регион; });
		sl.id_Region= region.id_Region;
	}

	transport.create= function(sl_record)
	{
		var sl = {id_Region:1};
		copy_to_db_record(sl_record, sl);
		ql.insert_with_next_id(db.sub_level, 'id_Sub_level', sl);
	}

	transport.update = function (id_Sub_level, sl_record)
	{
		var sl = ql.find_first_or_null(db.sub_level, function (r) { return r.id_Sub_level == id_Sub_level; });
		copy_to_db_record(sl_record, sl);
	}

	transport.read= function(id_Sub_level)
	{
		var res = ql.from(db.sub_level,'sl')
		.inner_join(db.region, 'rg', function (r) { return r.sl.id_Region==r.rg.id_Region; })
		.where(function (r) { return r.sl.id_Sub_level == id_Sub_level; })
		.map(function (r)
		{
			return {
				Дата: r.sl.StartDate
				,Регион: r.rg.OKATO
				, Прожиточный_минимум: {
					Подушевой: { Задан: (r.sl.Sum_Common && null!=r.sl.Sum_Common), Значение:r.sl.Sum_Common }
					,Трудоспособных: { Задан: (r.sl.Sum_Employable && null!=r.sl.Sum_Employable), Значение:r.sl.Sum_Employable }
					,Пенсионеров: { Задан: (r.sl.Sum_Pensioner && null!=r.sl.Sum_Pensioner), Значение:r.sl.Sum_Pensioner }
					,Несовершеннолетних: { Задан: (r.sl.Sum_Infant && null!=r.sl.Sum_Infant), Значение:r.sl.Sum_Infant }
				}
				, Регламентирующий_акт: {
					Название: r.sl.Reglament_Title
					,Дата: r.sl.Reglament_Date
					,Ссылка: r.sl.Reglament_Url
				}
			};
		})
		[0];
		return res;
	}

	transport.delete= function(ids)
	{
		var id_arr = ids.split(',');
		for (var i = 0; i < id_arr.length; i++)
		{
			var id_Sub_level = id_arr[i];
			db.sub_level = ql.delete(db.sub_level, function (sl) { return id_Sub_level == sl.id_Sub_level; });
		}
	}

	return transport.prepare_try_to_prepare_send_abort();
});
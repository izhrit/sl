﻿define([
	  'forms/base/ajax/ajax-jqGrid'
	, 'forms/sl/tests/db_sl'
	, 'forms/base/ql'
]
, function (ajax_jqGrid, db, ql)
{
	var transport = ajax_jqGrid();

	transport.options.url_prefix = 'sl?action=sl.jqgrid';

	transport.rows_all = function ()
	{
		return ql.from(db.sub_level,'sl')
			.inner_join(db.region, 'rg', function (r) { return r.sl.id_Region==r.rg.id_Region; })
			.map(function (r)
			{
				return {
					id:r.sl.id_Sub_level

					,Регион:r.rg.Name
					,Дата:r.sl.StartDate

					,Общий:r.sl.Sum_Common
					,Трудоспособных:r.sl.Sum_Employable
					,Пенсионеров:r.sl.Sum_Pensioner
					,Несовершеннолетних:r.sl.Sum_Infant

					,Публичность:(r.sl.Source_ContractNumber && null!=r.sl.Source_ContractNumber)?1:0
				};
			});
	}

	return transport.prepare_try_to_prepare_send_abort();
}
);
﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/sl/table/e_sl_table.html'
	, 'forms/sl/record/c_sl_record'
	, 'forms/base/h_msgbox'
	, 'tpl!forms/sl/table/e_sl_table_delete_confirm.html'
	, 'forms/base/h_numbering'
	, 'forms/base/h_number_format'
],
function (c_fastened, tpl, c_sl_record, h_msgbox, tpl_remove_confirm, h_numbering, h_number_format)
{
	return function (options_arg)
	{
		var controller = c_fastened(tpl);

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'sl');

		controller.base_crud_url = controller.base_url + '?action=sl.crud';
		controller.base_grid_url = controller.base_url + '?action=sl.jqgrid';

		var base_Render= controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this,sel);
			this.RenderGrid();

			var self= this;
			$(sel + ' a.button.add').button().click(function(e){e.preventDefault();self.OnAdd();});
			$(sel + ' a.button.edit').button().click(function(e){e.preventDefault();self.OnEdit();});
			$(sel + ' a.button.remove').button().click(function (e) { e.preventDefault(); self.OnDelete(); });
		}

		var moneySumFormatter = function (cellvalue, options, rowObject)
		{
			return h_number_format(cellvalue, 2, ' ,', ' ');
		}

		var publicFormatter = function (cellvalue, options, rowObject)
		{
			return 0==cellvalue ? '' : 'всем';
		}

		controller.colModel =
		[
			  { name: 'id', hidden: true }

			, { label: 'Регион', name: 'Регион', width: '400px', defaultSearch: 'cn', searchoptions: { sopt: ['cn', 'nc', 'bw', 'bn', 'ew', 'en'] } }
			, { label: 'C даты', name: 'Дата', align:'center', defaultSearch: 'cn', searchoptions: { sopt: ['cn', 'nc', 'bw', 'bn', 'ew', 'en'] } }

			, { label: 'Общий', name: 'Общий', align:'right', formatter: moneySumFormatter, defaultSearch: 'cn', searchoptions: { sopt: ['cn', 'nc', 'bw', 'bn', 'ew', 'en'] } }
			, { label: 'Трудосп.', name: 'Трудоспособных', align:'right', formatter: moneySumFormatter, defaultSearch: 'cn', searchoptions: { sopt: ['cn', 'nc', 'bw', 'bn', 'ew', 'en'] } }
			, { label: 'Пенсионер.', name: 'Пенсионеров', align:'right', formatter: moneySumFormatter, defaultSearch: 'cn', searchoptions: { sopt: ['cn', 'nc', 'bw', 'bn', 'ew', 'en'] } }
			, { label: 'Несоверш.', name: 'Несовершеннолетних', align:'right', formatter: moneySumFormatter, defaultSearch: 'cn', searchoptions: { sopt: ['cn', 'nc', 'bw', 'bn', 'ew', 'en'] } }

			, { label: 'Публично?', name: 'Публичность', align:'center', formatter: publicFormatter, searchoptions: { sopt:['eq'], value: ':;1:для всех;0:приватно'}, stype: 'select' }
		];

		controller.PrepareUrl = function ()
		{
			return this.base_grid_url;
		}

		controller.RenderGrid= function()
		{
			var sel = this.fastening.selector;
			var self= this;

			var url= self.PrepareUrl();
			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: self.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: 'Записи {0} - {1} из {2}'
				, emptyText: 'Нет записей для просмотра'
				, rownumbers: false
				, rowNum: 10
				, rowList: [5, 10, 15]
				, pager: '#cpw-form-sl-grid-pager'
				, viewrecords: true
				, height: 'auto'
				, width: '1000'
				, multiselect: true
				, multiboxonly: true
				, multiselectWidth: 35
				, ignoreCase: true
				, ondblClickRow: function () { self.OnEdit(); }
				, onSelectRow: function () { self.OnSelect(); }
				, onSelectAll: function () { self.OnSelect(); }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка прожиточныех минимумов", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.OnSelect = function ()
		{
			this.UpdateButtonsState();
		}

		controller.UpdateButtonsState = function ()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selected = grid.jqGrid('getGridParam', 'selarrrow').length;
			$(sel + ' a.button.edit').css('display', (1 == selected) ? 'inline-block' : 'none');
			$(sel + ' a.button.remove').css('display', (0 != selected) ? 'inline-block' : 'none');
		}

		controller.OnEdit= function()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var self= this;
			var selected = grid.jqGrid('getGridParam', 'selarrrow');
			if (1 == selected.length)
			{
				var id_Sub_level= selected[0];
				var ajaxurl_get= self.base_crud_url + '&cmd=get&id=' + id_Sub_level;
				var v_ajax = h_msgbox.ShowAjaxRequest("Получение записи с сервера", ajaxurl_get);
				v_ajax.ajax
				({
					dataType: "json", type: 'GET', cache: false
					, success: function (sl_record, textStatus)
					{
						if (null == sl_record)
						{
							v_ajax.ShowAjaxError(sl_record, textStatus);
						}
						else
						{
							self.DoEdit(id_Sub_level,sl_record);
						}
					}
				});
			}
		}

		controller.DoEdit = function (id_Sub_level,sl_record)
		{
			var sel= this.fastening.selector;
			var self= this;
			var cc_sl_record= c_sl_record();
			cc_sl_record.SetFormContent(sl_record);
			var btnOk= 'Сохранить запись о прожиточном минимуме';
			h_msgbox.ShowModal
			({
				  title:'Редактирование записи'
				, controller: cc_sl_record
				, buttons:[btnOk, 'Отмена']
				, onclose: function(btn)
				{
					if (btn==btnOk)
					{
						var ajaxurl_update= self.base_crud_url + '&cmd=update&id=' + id_Sub_level;
						v_ajax = h_msgbox.ShowAjaxRequest("Сохранение записи на сервере", ajaxurl_update);
						v_ajax.ajax
							({
								dataType: "json", type: 'POST', cache: false, data: cc_sl_record.GetFormContent()
								, success: function (responce)
								{
									if (responce.ok)
									{
										self.ReloadGrid();
										$(sel).trigger('model_change');
									}
								}
							});
					}
				}
			});
		}

		controller.OnAdd= function()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var self= this;
			var cc_sl_record= c_sl_record();
			cc_sl_record.SetFormContent({});
			var btnOk= 'Сохранить запись о прожиточном минимуме';
			h_msgbox.ShowModal
			({
				  title:'Добавление записи'
				, controller: cc_sl_record
				, width:400
				, buttons:[btnOk, 'Отмена']
				, onclose: function(btn)
				{
					if (btn==btnOk)
					{
						var ajaxurl_create= self.base_crud_url + '&cmd=add';
						v_ajax = h_msgbox.ShowAjaxRequest("Сохранение записи на сервере", ajaxurl_create);
						v_ajax.ajax
							({
								dataType: "json", type: 'POST', cache: false, data: cc_sl_record.GetFormContent()
								, success: function (responce)
								{
									if (responce)
									{
										self.ReloadGrid();
										$(sel).trigger('model_change');
									}
								}
							});
					}
				}
			});
		}

		controller.ReloadGrid= function()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			grid.trigger('reloadGrid');
			this.UpdateButtonsState();
		}

		controller.OnDelete= function()
		{
			var self = this;
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selected = grid.jqGrid('getGridParam', 'selarrrow');
			if (0 != selected.length)
			{
				var rows_to_delete= [];
				for (var i= 0; i<selected.length; i++)
					rows_to_delete.push(grid.jqGrid('getRowData', selected[i]));
				var to_delete = selected;
				var btnOk= 'Да, удалить ' + selected.length + h_numbering(selected.length,' запись',' записи',' записей');
				h_msgbox.ShowModal
				({
					  html: tpl_remove_confirm(rows_to_delete)
					, title: 'Подтверждение удаления'
					, width: '500'
					, buttons: [btnOk, 'Нет']
					, onclose: function (bname)
					{
						if (btnOk == bname)
						{
							var ajaxurl_create= self.base_crud_url + '&cmd=delete&id=' + to_delete.join(',');
							v_ajax = h_msgbox.ShowAjaxRequest("Удаление записей на сервере", ajaxurl_create);
							v_ajax.ajax
								({
									dataType: "json", type: 'POST', cache: false
									, success: function (responce)
									{
										if (responce)
										{
											self.ReloadGrid();
											$(sel).trigger('model_change');
										}
									}
								});
						}
					}
				});
			}
		}

		return controller;
	}
});
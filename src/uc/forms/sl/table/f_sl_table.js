define(['forms/sl/table/c_sl_table'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'table'
		, Title: 'Таблица прожиточных минимумов'
	};
	return form_spec;
});

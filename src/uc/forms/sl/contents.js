
/* ВНИМАНИЕ! */
/* данный файл генерируется автоматически */
/* НЕ редактируйте этот файл вручную для избежания конфликтов! */

/* ATTENTION! */
/* This file is generated automatically */
/* Do not edit this file manually to prevent conflicts! */

define
(
	[
		  'forms/base/collector'
		, 'txt!forms/sl/record/tests/contents/01sav.json.etalon.txt'
	],
	function (collect)
	{
		return collect([
		  'record-01sav'
		], Array.prototype.slice.call(arguments,1));
	}
);
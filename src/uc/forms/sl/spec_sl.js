define([
	  'forms/base/h_spec'
]
, function (h_spec)
{
	var items_spec = {

		controller:
		{
			"record":
			{
				path: 'forms/sl/record/c_sl_record'
				, title: 'Запись о прожиточном минимуме'
			}
			,"table":
			{
				path: 'forms/sl/table/c_sl_table'
				, title: 'База прожиточных минимумов'
			}
		}

		, content: {
		  "record-01sav":   { path: 'txt!forms/sl/record/tests/contents/01sav.json.etalon.txt' }
		}
	};
	return h_spec.combine(items_spec);
});
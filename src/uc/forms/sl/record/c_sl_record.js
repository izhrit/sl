define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/sl/record/e_sl_record.html'
	, 'forms/sl/base/h_region'
],
function (c_fastened, tpl, h_region)
{
	return function ()
	{
		var controller = c_fastened(tpl, {regions:h_region});

		controller.size = {width:520, height:500};

		var base_Render= controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this,sel);

			var self= this;
			$(sel + ' button.gotourl').click(function () { self.OnGotoUrl(); });
			$(sel + ' select[model-selector="Регион"]').select2();

			this.FixKvartalByDate();

			$(sel + ' .kvartal').change(function () { self.OnChangeKvartal(); });
			$(sel + ' [model-selector="Дата"]').change(function () { self.FixKvartalByDate(); });
		}

		controller.OnChangeKvartal = function ()
		{
			var sel= this.fastening.selector;
			var dt= $(sel + ' .kvartal').val();
			$(sel + ' [model-selector="Дата"]').val(dt);
		}

		controller.FixKvartalByDate = function ()
		{
			var sel= this.fastening.selector;
			var dt= $(sel + ' [model-selector="Дата"]').val();
			$(sel + " .kvartal > option").each(function()
			{
				if (dt == this.value)
				{
					$(this).attr('selected','selected');
					return false;
				}
			});
		}

		controller.OnGotoUrl = function ()
		{
			var sel= this.fastening.selector;
			var url= $(sel + ' input[model-selector="Регламентирующий_акт.Ссылка"]').val();
			if (url && null != url && '' != null)
			{
				if (0!=url.indexOf('http'))
					url= 'http://' + url;
				window.open(url);
			}
		}

		return controller;
	}
});
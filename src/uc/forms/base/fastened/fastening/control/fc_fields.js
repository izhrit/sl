﻿define([
	  'forms/base/fastened/fastening/fc_abstract'
	, 'forms/base/fastened/fastening/h_fastening_clip'
	, 'forms/base/h_get_selector'
]
, function (fc_abstract, h_fastening_clip, h_get_selector)
{
	var fc_fields = fc_abstract();

	fc_fields.match = function (adom_item, tag_name, fc_type)
	{
		return 'DIV' == tag_name && 'fields' == fc_type;
	}

	fc_fields.render = function (options, model, model_selector, adom_item, fc_data)
	{
		if (!options)
		{
			var dom_item = $(adom_item);
			dom_item.html(dom_item.html() + '<br/>absent options for "' + model_selector + '"!');
		}
		else if (!options.controller)
		{
			var dom_item = $(adom_item);
			dom_item.html(dom_item.html() + '<br/>absent controller for "' + model_selector + '"!');
		}
		else
		{
			var controller = options.controller();

			var dom_item_selector = h_get_selector($(adom_item), true);
			if (!model)
			{
				controller.CreateNew(dom_item_selector);
			}
			else
			{
				controller.SetFormContent(model);
				controller.Edit(dom_item_selector);
			}

			if (!fc_data)
				fc_data = {};

			fc_data.afc_type = 'fields';
			fc_data.amodel_selector = model_selector;
			fc_data.controller = controller;
		}
		return fc_data;
	}

	fc_fields.load_from_model = function (model, model_selector, dom_item, fc_data)
	{
		var controller = fc_data.controller;
		return fc_data;
	}

	fc_fields.save_to_model = function (model, model_selector, dom_item, fc_data)
	{
		var controller = fc_data.controller;
		var fields = controller.GetFormContent();

		for (var field_name in fields)
			model = h_fastening_clip.set_model_field_value(model, field_name, fields[field_name]);

		return model;
	}

	fc_fields.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.fields_control = function (model_selector)
		{
			return this.fastened_div(model_selector, 'fields');
		}
	}

	return fc_fields;
});
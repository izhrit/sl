﻿define([
	  'forms/base/fastened/fastening/fc_abstract'
	, 'forms/base/fastened/fastening/h_fastening_clip'
]
, function (fc_abstract, h_fastening_clip)
{
	var fc_array_item = fc_abstract();

	var fc_type_array_item = 'array-item';

	fc_array_item.match = function (adom_item, tag_name, fc_type)
	{
		return fc_type_array_item == fc_type;
	}

	fc_array_item.save_to_model = function (model, index, dom_item, fc_data)
	{
		var self = this;
		var fc_model = model[index];
		if (!fc_model || null == fc_model)
			fc_model = {};
		h_fastening_clip.each_first_level_fastening(dom_item, function (child_dom_item)
		{
			var child_fc_methods = self.h_fastening_clips.find_methods_for(child_dom_item);
			var child_fc_id = child_dom_item.attr('fc-id');
			var child_model_selector = child_dom_item.attr('model-selector');
			var child_fc_data = !fc_data ? null : fc_data[child_fc_id];
			fc_model= child_fc_methods.save_to_model(fc_model, child_model_selector, child_dom_item, child_fc_data);
		});
		model[index] = fc_model;
		return model;
	}

	fc_array_item.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.item_attrs = function ()
		{
			var context = this.current_context.parent_context;
			var array_model = context.model;
			if (-1 == array_model.index)
			{
				return this.fastening_attrs('seed', fc_type_array_item, context.fastening) + ' style="display:none" ';
			}
			else
			{
				var classes = null;
				if (this.options && this.options.BuildClassesForItem)
					classes = this.options.BuildClassesForItem(array_model.value, array_model.index);
				return this.fastening_attrs('[' + array_model.index + ']', fc_type_array_item, context.fastening) +
					(null == classes ? '' : ' class="' + classes + '" ') + ' array-index="' + array_model.index + '" ';
			}
		}
	}

	return fc_array_item;
});
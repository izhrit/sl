﻿define([
	'forms/base/h_msgbox'
],
function (h_msgbox)
{
	return function(settings, selector)
	{
		var helper = { settings: settings };

		if (selector)
			helper.settings.selector= selector;

		helper.prepare_button_ok_title = function (saction)
		{
			return (saction && saction.btn_ok_title)
				? saction.btn_ok_title
				: 'Сохранить' + this.safe_Кого_что_элемент();
		}

		helper.prepare_id_div = function (saction,postfix)
		{
			if (saction && saction.id_div)
			{
				return saction.id_div;
			}
			else
			{
				var settings = this.settings;
				var id_div = (!settings.id_div_prefix ? 'cpw-collection-item' : settings.id_div_prefix) + postfix;
				return id_div;
			}
		}

		helper.safe_Кого_что_элемент = function ()
		{
			var settings = this.settings;
			if (!settings.Кого_что_элемент && !settings.О_ком_о_чём)
			{
				return '';
			}
			else
			{
				var res = !settings.Кого_что_элемент ? ' запись' : (' ' + settings.Кого_что_элемент);
				if (settings.О_ком_о_чём)
					res+= ' ' + settings.О_ком_о_чём;
				return res;
			}
		}

		helper.safe_Кого_чего_элемента = function ()
		{
			var settings = this.settings;
			if (!settings.Кого_чего_элемента && !settings.О_ком_о_чём)
			{
				return '';
			}
			else
			{
				var res = !settings.Кого_чего_элемента ? ' записи' : (' ' + settings.Кого_чего_элемента);
				if (settings.О_ком_о_чём)
					res+= ' ' + settings.О_ком_о_чём;
				return res;
			}
		}

		helper.prepare_form_title = function (saction, prefix, postfix)
		{
			if (saction && saction.title)
			{
				return saction.title;
			}
			else
			{
				var title = prefix + this.safe_Кого_чего_элемента();
				if (postfix)
					title+= postfix;
				return title;
			}
		}

		helper.OnAdd = function (e)
		{
			var settings = this.settings;
			var readonly = settings.controller.options && settings.controller.options.readonly;
			if (readonly)
				return;
			var sadd= settings.add;
			var controller= settings.controller;
			var fastening= controller.fastening;
			var sel = fastening.selector;
			if (settings.selector)
				sel += ' ' + settings.selector;

			if (!settings.item_controller)
			{
				var fc_dom_item= $(sel);
				var model= fastening.get_fc_model_value(fc_dom_item);
				model.push({});
				fastening.set_fc_model_value(fc_dom_item, model);
				fastening.on_after_render();
				fc_dom_item.trigger('model_change');
			}
			else
			{
				var item_controller= settings.item_controller(e);
				var btnOk = this.prepare_button_ok_title(sadd);
				h_msgbox.ShowModal
				({
					  title: this.prepare_form_title(sadd,'Добавление')
					, controller: item_controller
					, buttons: [btnOk, 'Отмена']
					, id_div: this.prepare_id_div(sadd,'-add-form')
					, onclose: function(btn)
					{
						if (btn==btnOk)
						{
							var fc_dom_item= $(sel);
							var model= fastening.get_fc_model_value(fc_dom_item);
							model.push(item_controller.GetFormContent());
							fastening.set_fc_model_value(fc_dom_item, model);
							fastening.on_after_render();
							fc_dom_item.trigger('model_change');
						}
					}
				});
			}
		}

		helper.OnEdit = function (e)
		{
			var settings = this.settings;
			var readonly = settings.controller.options && settings.controller.options.readonly;
			var sedit= settings.edit;
			var controller= settings.controller;
			var fastening = controller.fastening;
			var sel = fastening.selector;
			if (settings.selector)
				sel+= ' ' + settings.selector;

			var t = $(e.target);
			if ('array-item' != t.attr('fc-type'))
				t = t.parents('[fc-type="array-item"]');
			var i_item = parseInt(t.attr('array-index'));

			var fc_dom_item= $(sel);
			var model = fastening.get_fc_model_value(fc_dom_item);

			var item_controller= settings.item_controller();
			item_controller.SetFormContent(model[i_item]);
			var btnOk= this.prepare_button_ok_title(sedit);
			h_msgbox.ShowModal
			({
				  title: this.prepare_form_title(sedit,readonly ? 'Просмотр' : 'Редактирование')
				, controller: item_controller
				, buttons: readonly ? ['Закрыть'] : [btnOk, 'Отмена']
				, id_div: this.prepare_id_div(sedit,'-edit-form')
				, onclose: function(btn)
				{
					if (btn==btnOk)
					{
						var model= fastening.get_fc_model_value(fc_dom_item);
						model[i_item]= item_controller.GetFormContent();
						fastening.set_fc_model_value(fc_dom_item, model);
						fastening.on_after_render();
						fc_dom_item.trigger('model_change');
					}
				}
			});
		}

		helper.prepare_item_text= function (title, i_item, item)
		{
			if ('string' == typeof title)
			{
				return title.replace('#item_number#', i_item + 1);
			}
			else
			{
				return title(i_item, item);
			}
		}

		helper.prepare_delete_html = function (i_item, item)
		{
			var settings = this.settings;
			var sdelete = settings.delete;
			if (sdelete && sdelete.html)
			{
				return this.prepare_item_text(sdelete.html, i_item, item);
			}
			else
			{
				var html = 'Вы действительно хотите удалить' + this.safe_Кого_что_элемент();
				html += ' №' + (i_item + 1);
				if (settings.short_item_description_html)
				{
					html+= '<p style="text-align:center;"><b>';
					html+= settings.short_item_description_html(item,i_item);
					html+= '</b>?</p>';
				}
				return html;
			}
			
		}

		helper.OnDelete = function (e)
		{
			e.stopPropagation();
			var i_item = parseInt($(e.target).parents('[fc-type="array-item"]').attr('array-index'));

			var settings = this.settings;
			var readonly = settings.controller.options && settings.controller.options.readonly;
			if (readonly)
				return;
			var sdelete= settings.delete;
			var controller = settings.controller;
			var fastening = controller.fastening;
			var sel = fastening.selector;
			if (settings.selector)
				sel+= ' ' + settings.selector;
			var fc_dom_item = $(sel);
			var model = fastening.get_fc_model_value(fc_dom_item);
			var item = model[i_item];
			var btnOk = 'Да, удалить';
			h_msgbox.ShowModal
			({
				title: this.prepare_item_text(this.prepare_form_title(sdelete,'Подтверждение удаления',' №#item_number#'),i_item,item)
				, html: this.prepare_delete_html(i_item, item)
				, width: (!sdelete || !sdelete.width) ? 800 : sdelete.width
				, height: (!sdelete || !sdelete.height) ? 220 : sdelete.height
				, id_div: this.prepare_id_div(sdelete,'-confirm-delete-form')
				, buttons: [btnOk, 'Нет, не удалять']
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						model.splice(i_item, 1);
						fastening.set_fc_model_value(fc_dom_item, model);
						fc_dom_item.trigger('model_change');
					}
				}
			});
		}

		helper.static_OnAdd = function (e) { return helper.OnAdd(e); }
		helper.static_OnEdit = function (e) { return helper.OnEdit(e); }
		helper.static_OnDelete = function (e) { return helper.OnDelete(e); }

		helper.FastenAction = function (sel, eventType, static_handler)
		{
			$(sel).unbind(eventType, static_handler).bind(eventType, static_handler);
			return this;
		}

		helper.FastenEdit = function (sel, eventType)
		{
			return this.FastenAction(sel, eventType, this.static_OnEdit);
		}

		helper.FastenDelete = function (sel, eventType)
		{
			return this.FastenAction(sel, eventType, this.static_OnDelete);
		}

		helper.FastenAdd = function (sel, eventType)
		{
			return this.FastenAction(sel, eventType, this.static_OnAdd);
		}

		helper.Fasten = function (sel,settings)
		{
			if (!sel)
				sel = this.settings.controller.fastening.selector;
			if (this.settings.selector)
				sel+= ' ' + this.settings.selector;
			if (!settings)
				settings= { add: 'a.add', edit: 'a.edit', delete:'a.delete'};
			if (settings.add)
			{
				var sadd = ('string' != typeof settings.add) ? settings.add : { selector:settings.add };
				this.FastenAdd(
					sel + ' ' + sadd.selector,
					!sadd.eventType ? 'click' : sadd.eventType
				);
			}
			if (settings.edit)
			{
				var sedit = ('string' != typeof settings.edit) ? settings.edit : { selector: settings.edit };
				var old_on_after_render1 = this.settings.controller.fastening.on_after_render;
				var self= this;
				this.settings.controller.fastening.on_after_render = function ()
				{
					if (old_on_after_render1)
						old_on_after_render1();
					self.FastenEdit(
						sel + ' ' + sedit.selector,
						!sedit.eventType ? 'click' : sedit.eventType
					);
				}
			}
			if (settings.delete)
			{
				var old_on_after_render2 = this.settings.controller.fastening.on_after_render;
				var self= this;
				this.settings.controller.fastening.on_after_render = function ()
				{
					if (old_on_after_render2)
						old_on_after_render2();
					var sdelete = ('string' != typeof settings.delete) ? settings.delete : { selector: settings.delete };
					self.FastenDelete(
						sel + ' ' + sdelete.selector,
						!sdelete.eventType ? 'click' : sedit.eventType
					);
				}
			}
		}

		return helper;
	}
});

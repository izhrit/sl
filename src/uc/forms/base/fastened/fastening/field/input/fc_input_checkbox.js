﻿define([
	  'forms/base/fastened/fastening/fc_abstract'
	, 'forms/base/fastened/fastening/h_fastening_clip'
]
, function (fc_abstract, h_fastening_clip)
{
	var fc_checkbox_field = fc_abstract();

	fc_checkbox_field.match = function (adom_item, tag_name, fc_type)
	{
		var dom_item = $(adom_item);
		return 'INPUT' == tag_name && 'checkbox' == dom_item.attr('type');
	}

	fc_checkbox_field.load_from_model = function (model, model_selector, dom_item, fc_data)
	{
		var value = h_fastening_clip.get_model_field_value(model, model_selector);
		if (true == value || 'true' == value)
		{
			$(dom_item).attr('checked', 'checked');
		}
		else
		{
			$(dom_item).attr('checked', null);
		}
		return fc_data;
	}

	fc_checkbox_field.save_to_model = function (model, model_selector, dom_item, fc_data)
	{
		var value = 'checked' == $(dom_item).attr('checked');
		return h_fastening_clip.set_model_field_value(model, model_selector, value);
	}

	fc_checkbox_field.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.checkbox_attrs = function (model_selector, default_value)
		{
			var res = this.fastening_attrs(model_selector);
			var actual_value = this.value();
			if (true === actual_value || 'true' === actual_value)
			{
				res += " checked='checked'";
			}
			else if (false === actual_value || 'false' === actual_value)
			{
			}
			else if (true == default_value)
			{
				res += " checked='checked'";
			}
			else
			{
			}
			return res;
		}
	}

	return fc_checkbox_field;
});
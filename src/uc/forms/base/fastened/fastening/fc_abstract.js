﻿define(function ()
{
	var h_fastening_clips = {};
	return function ()
	{
		var fastening_clip=
		{
			h_fastening_clips: h_fastening_clips
			, match: function (dom_item, tag_name, fc_type) { return false; }
			, render: function (options, model, model_selector, dom_item, fc_data) { return fc_data; }
			, load_from_model: function (model, model_selector, dom_item, fc_data) { return fc_data; }
			, save_to_model: function (model, model_selector, dom_item, fc_data) { return model; }
			, destroy: function (dom_item, fc_data) { }
			, add_template_argument_methods: function (_template_argument) { }
		};
		return fastening_clip;
	}

});
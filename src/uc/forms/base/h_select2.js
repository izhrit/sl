define(function ()
{
	var helper = {};

	helper.FindForSelect = function (list, term)
	{
		var res = [];
		var fixed_term = term.toUpperCase();
		for (var i = 0; i < list.length; i++)
		{
			var row = list[i];
			if (-1!=row.text.toUpperCase().indexOf(fixed_term))
				res.push(row);
			if (res.length > 20)
				break;
		}
		return res;
	}

	helper.FindById = function (list, id)
	{
		for (var i = 0; i < list.length; i++)
		{
			var row = list[i];
			if (id == row.id)
				return row;
		}
		return null;
	}

	return helper;
});

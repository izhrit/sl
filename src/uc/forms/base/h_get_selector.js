﻿define(function ()
{
	var get_selector = function (element)
	{
		var pieces = [];

		for (; element && element.tagName !== undefined; element = element.parentNode)
		{
			if (element.className)
			{
				var classes = element.className.split(' ');
				for (var i in classes)
				{
					if (classes.hasOwnProperty(i) && classes[i])
					{
						pieces.unshift(classes[i]);
						pieces.unshift('.');
					}
				}
			}

			if (element.id && !/\s/.test(element.id))
			{
				pieces.unshift(element.id);
				pieces.unshift('#');
			}

			pieces.unshift(element.tagName);
			pieces.unshift(' > ');
		}
		return pieces.slice(1).join('');
	};

	return function (el, only_one)
	{
		if (true === only_one)
		{
			return get_selector(el[0]);
		}
		else
		{
			return $.map(el, function (el)
			{
				return get_selector(el);
			});
		}
	};
});
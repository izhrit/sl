define([
	  'forms/base/codec/codec'
],
function (BaseCodec)
{
	return function()
	{
		var codec= BaseCodec();

		codec.Encode = function (data)
		{
			var xml_string = '<?xml version="1.0" encoding="utf-8"?>\r\n';
			xml_string += '<data xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"\r\n\t>';
			xml_string += data;
			xml_string += '</data>';
			return xml_string;
		};

		codec.Decode = function (xml_string)
		{
			var doc = null;
			try
			{
				doc = new ActiveXObject('Microsoft.XMLDOM');
				doc.async = 'false';
				doc.loadXML(xml_string);
			}
			catch (ex)
			{
				doc = ex;
			}
			if (!doc || !doc.documentElement || doc.getElementsByTagName('parsererror').length)
			{
				return null;
			}
			else
			{
				return doc.documentElement.text;
			}
		};

		return codec;
	}
});

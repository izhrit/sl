include ..\..\..\..\..\..\..\wbt.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions

wait_text "Ученики 1 - 10 из 30"
shot_check_png ..\..\shots\00new.png

type_id gs_Ученик Сидоров
wait_text "Ученики 1 - 10 из 10"
shot_check_png ..\..\shots\00new_Sidorov.png
type_id gs_Ученик ""

type_id gs_Алгебра 2
wait_text "Ученики 1 - 10 из 20"
shot_check_png ..\..\shots\00new_Algebra_2.png
type_id gs_Алгебра ""

type_id gs_Пение 5
wait_text "Ученики 1 - 10 из 10"
shot_check_png ..\..\shots\00new_Penie_5.png
type_id gs_Пение ""

wait_text "Ученики 1 - 10 из 30"
wait_click_class ui-icon-seek-next
wait_text "Ученики 11 - 20 из 30"
shot_check_png ..\..\shots\00new_2.png
wait_click_class ui-icon-seek-prev
wait_text "Ученики 1 - 10 из 30"
shot_check_png ..\..\shots\00new.png

wait_click_id jqgh_crowbar-test-grid_Ученик
shot_check_png ..\..\shots\00new_order_by_uchenik.png

exit
﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/test/fastened/containers/e_containers.html'
	, 'forms/test/fastened/simple/c_simple'
],
function (c_fastened, tpl, c_simple)
{
	return function ()
	{
		var options =
		{
			field_spec:
			{
				  Сам: { controller: c_simple }
				, Шеф: { controller: c_simple }
			}
		};
		var controller = c_fastened(tpl, options);
		return controller;
	}
});
include ..\..\..\..\..\..\wbt.lib.txt quiet
include ..\in.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions
wait_text "select"

check_stored_lines select2_fields_1

je $(".select2-container").removeClass("select2-container-active");
shot_check_png ..\..\shots\01sav.png

play_stored_lines select2_fields_2

je $(".select2-container").removeClass("select2-container-active");
shot_check_png ..\..\shots\02edt.png

check_stored_lines select2_fields_2

wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\02edt.json.result.txt
exit
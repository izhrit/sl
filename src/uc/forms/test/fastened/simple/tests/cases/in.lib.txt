start_store_lines_as simple_fields_1

  js wbt.SetModelFieldValue("Ученик", "Иванов");
  js wbt.SetModelFieldValue("Оценки.алгебра", "2");
  js wbt.SetModelFieldValue("Оценки.пение", "2");

stop_store_lines

start_store_lines_as simple_fields_2

  js wbt.SetModelFieldValue("Ученик", "Петров");
  js wbt.SetModelFieldValue("Оценки.алгебра", "2");
  js wbt.SetModelFieldValue("Оценки.пение", "5");

stop_store_lines

start_store_lines_as simple_fields_3

  js wbt.SetModelFieldValue("Ученик", "Сидоров");
  js wbt.SetModelFieldValue("Оценки.алгебра", "3");
  js wbt.SetModelFieldValue("Оценки.пение", "3");

stop_store_lines

